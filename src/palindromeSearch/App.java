package palindromeSearch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

	public static void main(String[] args) {
		ArrayList<String> wordList = ReadInputFile();		
		ArrayList<Future<?>> futures = new ArrayList<>();
		m_bw = createOutputFile();
		dispatchTasks(wordList, futures, m_bw);
		waitForTaskCompletion(futures);
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.exit(0);
			}	
		}, 30000);
	}

	private static void waitForTaskCompletion(ArrayList<Future<?>> futures) {
		for (int i = 0; i < futures.size(); i++) {
			try {
				futures.get(i).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void dispatchTasks(ArrayList<String> wordList, ArrayList<Future<?>> futures, BufferedWriter bw) {
		ExecutorService servc = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		int taskRunners = Runtime.getRuntime().availableProcessors();
		int interval = wordList.size() / taskRunners;
		
		for (int i = 0; i < taskRunners; i++) {
			int start = i * interval;
			int end = 0;
			if ((start + interval) > wordList.size()) {
				end = wordList.size();
			} else if (i == (taskRunners - 1)) {
				end = wordList.size();
			} else {
				end = start + interval;
			}
			futures.add(servc.submit(new PermutationGenerator(wordList, 2, start, end, bw)));
		}
	}

	private static ArrayList<String> ReadInputFile() {
		try {
			InputStream res = App.class.getResourceAsStream("/words_alpha.txt");
			BufferedReader reader = new BufferedReader(new InputStreamReader(res));
			ArrayList<String> wordList = new ArrayList<>();
			String line = null;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				wordList.add(line);
			}
			reader.close();
			return wordList;
		} catch (FileNotFoundException fnf) {
			System.out.println(fnf.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		return null;
	}
	
	private static BufferedWriter createOutputFile() {
		try {
			File outputFile = new File("results.txt");
			FileOutputStream fos = new FileOutputStream(outputFile);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
			return writer;
		} catch (FileNotFoundException fnf) {
			System.out.println(fnf.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		return null;
	}
	
	private static BufferedWriter m_bw;

}
