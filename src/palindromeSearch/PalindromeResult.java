package palindromeSearch;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class PalindromeResult implements Runnable {

	public PalindromeResult(ArrayList<ArrayList<String>> palindromes, BufferedWriter bw) {
		for (ArrayList<String> palindrome : palindromes) {
			m_palindromes.add(palindrome);
		}
		m_bw = bw;
	}

	@Override
	public void run() {
		try {
			for (ArrayList<String> palindrome : m_palindromes) {
				for (String word : palindrome) {
					m_bw.write(word);
					m_bw.write(" ");
				}
				m_bw.newLine();
			}
			m_bw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	ArrayList<ArrayList<String>> m_palindromes = new ArrayList<>();
	BufferedWriter m_bw;
}
