package palindromeSearch;

import java.io.BufferedWriter;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

public class PermutationGenerator implements Runnable {

	public PermutationGenerator(ArrayList<String> wordList, int recursionDepth, int start, int end, BufferedWriter bw) {
		m_count = 0;
		m_wordList = wordList;
		m_recursionDepth = recursionDepth;
		m_start = start;
		m_end = end;
		m_bw = bw;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		ArrayList<String> words = new ArrayList<>();
		this.RecursiveGeneratorInitial(m_wordList, m_recursionDepth, words, m_start, m_end);
	}

	private void RecursiveGeneratorInitial(ArrayList<String> origList, int recursionToGo, ArrayList<String> current,
			int start, int stop) {
		for (int i = start; i < stop; i++) {
			current.add(origList.get(i));
			this.RecursiveGenerator(origList, recursionToGo - 1, current);
			current.subList(0, current.size()).clear();
		}
	}

	private void RecursiveGenerator(ArrayList<String> origList, int recursionToGo, ArrayList<String> current) {
		if (recursionToGo == 0) {
			BaseLevelTest(origList, current);
		} else {
			int deleteIndex = current.size();
			for (int i = 0; i < origList.size(); i++) {
				current.add(origList.get(i));
				this.RecursiveGenerator(origList, recursionToGo - 1, current);
				current.subList(deleteIndex, current.size()).clear();
			}
		}
	}

	final private void BaseLevelTest(ArrayList<String> origList, ArrayList<String> current) {
		StringBuilder forward = new StringBuilder();
		StringBuilder reverse = new StringBuilder();
		ArrayList<ArrayList<String>> palindromes = new ArrayList<ArrayList<String>>();
		for (String s : current) {
			forward.append(s);
		}
		int deleteIndex = forward.length();
		for (int i = 0; i < origList.size(); i++) {
			forward.append(origList.get(i));
			reverse.append(forward);
			reverse.reverse();
			if (forward.toString().equals(reverse.toString())) {
				m_count++;
				ArrayList<String> palindrome = new ArrayList<>();
				for (String s : current) {
					palindrome.add(s);
				}
				palindrome.add(m_wordList.get(i));
				palindromes.add(palindrome);
			}
			reverse.delete(0, reverse.length());
			forward.delete(deleteIndex, forward.length());
		}
		if(palindromes.size() != 0) {
			PalindromeResult pr = new PalindromeResult(palindromes, m_bw);
			SwingUtilities.invokeLater(pr);
		}
	}

	public int GetCount() {
		return m_count;
	}

	private int m_count;
	private ArrayList<String> m_wordList;
	private int m_recursionDepth;
	private int m_start;
	private int m_end;
	private BufferedWriter m_bw;
}
